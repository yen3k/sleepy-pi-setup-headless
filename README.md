Configuration script for a headless Raspberry Pi to operate with Sleepy Pi.

The original setup script from [Spell Foundry](https://raw.githubusercontent.com/SpellFoundry/Sleepy-Pi-Setup/master/Sleepy-Pi-Setup.sh) installs the Arduino IDE, whereas this script installs [Arduino CLI](https://github.com/arduino/arduino-cli)

After running the script one can build a project with:

```
arduino-cli compile -b arduino:avr:fio someSketch
```

Upload with:

```
arduino-cli upload -p /dev/ttyS0 --fqbn arduino:avr:fio someSKetch
```

Tested on:
* RPi 3 A+ (16-10-2020)

If you can confirm the script works for other types of raspberry please let me
know by writing a mail to sleepyheadlessscript@yenmail.party, and I will
update the list.
